package com.sankar.javaworks.quote.generator.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteGeneratorApp {

	public static void main(String[] args) {
		SpringApplication.run(QuoteGeneratorApp.class, args);
	}

}
